use std::path::PathBuf;

use crate::{Category, Location, Severity, TextRange};

pub struct Issue;
impl crate::Issue for Issue {
    fn analyzer_id(&self) -> String {
        "fake".to_owned()
    }

    fn issue_id(&self) -> String {
        "fake_issue".to_owned()
    }

    fn fingerprint(&self) -> md5::Digest {
        md5::compute("sum")
    }

    fn category(&self) -> Category {
        Category::Style
    }

    fn severity(&self) -> Severity {
        Severity::Info
    }

    fn location(&self) -> Option<Location> {
        let location = Location {
            path: PathBuf::from("Cargo.lock"),
            range: TextRange::new((1, 1), (2, 42)),
            message: "this issue is bad!".to_owned(),
        };
        Some(location)
    }
}
