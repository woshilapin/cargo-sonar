#![doc = include_str!("../../README.md")]

#[cfg(any(
    feature = "audit",
    feature = "clippy",
    feature = "deny",
    feature = "outdated",
    feature = "udeps",
))]
use clap::Parser as _;
use eyre::{Context as _, Result};
use tracing::Level;
use tracing_subscriber::{prelude::*, EnvFilter};

fn init_tracer() {
    let default_level = Level::INFO;
    let rust_log =
        std::env::var(EnvFilter::DEFAULT_ENV).unwrap_or_else(|_| default_level.to_string());
    let env_filter_subscriber = EnvFilter::try_new(rust_log).unwrap_or_else(|e| {
        eprintln!(
            "invalid {}, falling back to level '{}' - {}",
            EnvFilter::DEFAULT_ENV,
            default_level,
            e,
        );
        EnvFilter::new(default_level.to_string())
    });
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(env_filter_subscriber)
        .init();
}

fn main() -> Result<()> {
    init_tracer();
    color_eyre::install()?;
    // When calling `cargo sonar` binary, the first 2 arguments are
    // - `cargo-sonar`
    // - `sonar`
    // which is different from calling the binary directly.
    // If `sonar` is the second argument, we filter it out.
    let options = cargo_sonar::Command::parse_from(
        std::env::args_os()
            .enumerate()
            .filter(|&(i, ref args)| !(i == 1 && args == "codeclimate"))
            .map(|(_, args)| args),
    );

    let options = &options;
    let converter = cargo_sonar::Converter::try_new()?;
    let codeclimate = converter.codeclimatize(options)?;
    let file = std::fs::File::create(&options.codeclimate_path)
        .with_context(|| format!("failed to create '{:?}' file", &options.codeclimate_path))?;
    tracing::info!("{} codeclimate issues created", codeclimate.len());
    #[cfg(not(debug_assertions))]
    let writer = serde_json::to_writer;
    #[cfg(debug_assertions)]
    let writer = serde_json::to_writer_pretty;
    writer(file, &codeclimate).context(format!(
        "failed to write sonar issues to '{:?}' file",
        &options.codeclimate_path,
    ))?;

    Ok(())
}
