#![cfg_attr(feature = "document-features", doc = "Feature flags")]
#![cfg_attr(
    feature = "document-features",
    cfg_attr(doc, doc = ::document_features::document_features!())
)]
mod cargo;
pub use crate::cargo::Lockfile;
mod cli;
pub use cli::Command;

// Parsers
#[cfg(feature = "audit")]
pub mod audit;
#[cfg(feature = "clippy")]
pub mod clippy;
#[cfg(feature = "deny")]
pub mod deny;
#[cfg(feature = "outdated")]
pub mod outdated;
#[cfg(test)]
mod test;
#[cfg(feature = "udeps")]
pub mod udeps;

// Reporter
#[cfg(feature = "codeclimate")]
pub mod codeclimate;
#[cfg(feature = "codeclimate")]
pub use codeclimate::CodeClimate;
#[cfg(feature = "sonar")]
pub mod sonar;
#[cfg(feature = "sonar")]
pub use sonar::Sonar;

use eyre::Result;
use md5::Digest;
use std::{marker::PhantomData, path::PathBuf};

#[derive(Clone, Copy, Debug)]
#[non_exhaustive]
pub enum Severity {
    Blocker,
    Critical,
    Major,
    Minor,
    Info,
}

#[derive(Clone, Copy, Debug)]
#[non_exhaustive]
pub enum Category {
    Bug,
    Complexity,
    Duplication,
    Performance,
    Security,
    Style,
}

#[derive(Clone, Copy, Debug)]
struct Position {
    line: usize,
    column: usize,
}

#[derive(Clone, Copy, Debug)]
pub struct TextRange {
    start: Position,
    end: Position,
}

impl TextRange {
    #[inline]
    #[must_use]
    pub fn new(
        (start_line, start_column): (usize, usize),
        (end_line, end_column): (usize, usize),
    ) -> Self {
        Self {
            start: Position {
                line: start_line,
                column: start_column,
            },
            end: Position {
                line: end_line,
                column: end_column,
            },
        }
    }
}

impl Default for TextRange {
    #[inline]
    fn default() -> Self {
        Self::new((1, 0), (1, 0))
    }
}

#[derive(Clone, Debug)]
pub struct Location {
    pub path: PathBuf,
    pub range: TextRange,
    pub message: String,
}

pub trait Issue {
    /// Name of the analyzer which produce the issue
    fn analyzer_id(&self) -> String;
    /// Name of the issue
    fn issue_id(&self) -> String;
    /// Unique identifier of the issue. Default implementation concatenate
    /// `analyzer_id()` and `issue_id()`.
    #[inline]
    fn issue_uid(&self) -> String {
        format!("{}::{}", self.analyzer_id(), self.issue_id())
    }
    /// Fingerprint is used to track an issue over evolutions of the code.
    /// Ideally, even if the line changed but the issue is the same, the
    /// fingerprint should not change.
    fn fingerprint(&self) -> Digest;
    /// Type of the reported lint, issue or vulnerability
    fn category(&self) -> Category;
    /// Severity of the reported issue, can be used to prioritize the issues
    /// to fix.
    fn severity(&self) -> Severity;
    /// Location of the issue in a file. This information is crucial for nice
    /// display in UI.
    fn location(&self) -> Option<Location>;
    /// Optional other locations related to the same issue.
    #[inline]
    fn other_locations(&self) -> Vec<Location> {
        Vec::new()
    }
}

enum IssueType<'lock> {
    #[cfg(feature = "audit")]
    Audit(audit::Issue<'lock>),
    #[cfg(feature = "clippy")]
    Clippy(Box<clippy::Issue>),
    #[cfg(feature = "deny")]
    Deny(deny::Issue<'lock>),
    #[cfg(feature = "outdated")]
    Outdated(outdated::Issue<'lock>),
    #[cfg(feature = "udeps")]
    Udeps(udeps::Issue<'lock>),
    #[cfg(test)]
    Test(test::Issue),
    // When compiling only with the `clippy` feature,
    // 'lock lifetime on IssueType is not used by any variant.
    // Phantom variant is here for avoiding a compilation error in this case.
    #[doc(hidden)]
    #[allow(unused)]
    Phantom(PhantomData<&'lock ()>),
}

impl crate::Issue for IssueType<'_> {
    #[inline]
    fn analyzer_id(&self) -> String {
        match *self {
            #[cfg(feature = "audit")]
            IssueType::Audit(ref audit) => audit.analyzer_id(),
            #[cfg(feature = "clippy")]
            IssueType::Clippy(ref clippy) => clippy.analyzer_id(),
            #[cfg(feature = "deny")]
            IssueType::Deny(ref deny) => deny.analyzer_id(),
            #[cfg(feature = "outdated")]
            IssueType::Outdated(ref outdated) => outdated.analyzer_id(),
            #[cfg(feature = "udeps")]
            IssueType::Udeps(ref udeps) => udeps.analyzer_id(),
            #[cfg(test)]
            IssueType::Test(ref test) => test.analyzer_id(),
            // ALLOW: only for tests
            #[allow(clippy::unimplemented)]
            _ => unimplemented!(),
        }
    }

    #[inline]
    fn issue_id(&self) -> String {
        match *self {
            #[cfg(feature = "audit")]
            IssueType::Audit(ref audit) => audit.issue_id(),
            #[cfg(feature = "clippy")]
            IssueType::Clippy(ref clippy) => clippy.issue_id(),
            #[cfg(feature = "deny")]
            IssueType::Deny(ref deny) => deny.issue_id(),
            #[cfg(feature = "outdated")]
            IssueType::Outdated(ref outdated) => outdated.issue_id(),
            #[cfg(feature = "udeps")]
            IssueType::Udeps(ref udeps) => udeps.issue_id(),
            #[cfg(test)]
            IssueType::Test(ref test) => test.issue_id(),
            // ALLOW: only for tests
            #[allow(clippy::unimplemented)]
            _ => unimplemented!(),
        }
    }

    #[inline]
    fn fingerprint(&self) -> Digest {
        match *self {
            #[cfg(feature = "audit")]
            IssueType::Audit(ref audit) => audit.fingerprint(),
            #[cfg(feature = "clippy")]
            IssueType::Clippy(ref clippy) => clippy.fingerprint(),
            #[cfg(feature = "deny")]
            IssueType::Deny(ref deny) => deny.fingerprint(),
            #[cfg(feature = "outdated")]
            IssueType::Outdated(ref outdated) => outdated.fingerprint(),
            #[cfg(feature = "udeps")]
            IssueType::Udeps(ref udeps) => udeps.fingerprint(),
            #[cfg(test)]
            IssueType::Test(ref test) => test.fingerprint(),
            // ALLOW: only for tests
            #[allow(clippy::unimplemented)]
            _ => unimplemented!(),
        }
    }

    #[inline]
    fn category(&self) -> Category {
        match *self {
            #[cfg(feature = "audit")]
            IssueType::Audit(ref audit) => audit.category(),
            #[cfg(feature = "clippy")]
            IssueType::Clippy(ref clippy) => clippy.category(),
            #[cfg(feature = "deny")]
            IssueType::Deny(ref deny) => deny.category(),
            #[cfg(feature = "outdated")]
            IssueType::Outdated(ref outdated) => outdated.category(),
            #[cfg(feature = "udeps")]
            IssueType::Udeps(ref udeps) => udeps.category(),
            #[cfg(test)]
            IssueType::Test(ref test) => test.category(),
            // ALLOW: only for tests
            #[allow(clippy::unimplemented)]
            _ => unimplemented!(),
        }
    }

    #[inline]
    fn severity(&self) -> Severity {
        match *self {
            #[cfg(feature = "audit")]
            IssueType::Audit(ref audit) => audit.severity(),
            #[cfg(feature = "clippy")]
            IssueType::Clippy(ref clippy) => clippy.severity(),
            #[cfg(feature = "deny")]
            IssueType::Deny(ref deny) => deny.severity(),
            #[cfg(feature = "outdated")]
            IssueType::Outdated(ref outdated) => outdated.severity(),
            #[cfg(feature = "udeps")]
            IssueType::Udeps(ref udeps) => udeps.severity(),
            #[cfg(test)]
            IssueType::Test(ref test) => test.severity(),
            // ALLOW: only for tests
            #[allow(clippy::unimplemented)]
            _ => unimplemented!(),
        }
    }

    #[inline]
    fn location(&self) -> Option<Location> {
        match *self {
            #[cfg(feature = "audit")]
            IssueType::Audit(ref audit) => audit.location(),
            #[cfg(feature = "clippy")]
            IssueType::Clippy(ref clippy) => clippy.location(),
            #[cfg(feature = "deny")]
            IssueType::Deny(ref deny) => deny.location(),
            #[cfg(feature = "outdated")]
            IssueType::Outdated(ref outdated) => outdated.location(),
            #[cfg(feature = "udeps")]
            IssueType::Udeps(ref udeps) => udeps.location(),
            #[cfg(test)]
            IssueType::Test(ref test) => test.location(),
            // ALLOW: only for tests
            #[allow(clippy::unimplemented)]
            _ => unimplemented!(),
        }
    }
}

trait FromIssues<'lock> {
    type Report;
    fn from_issues(issues: impl IntoIterator<Item = IssueType<'lock>>) -> Self::Report;
}

pub struct Converter {
    // When feture 'clippy' is used alone, 'lockfile' is not used at all
    // Make it 'allow(unused)' in this case
    #[cfg_attr(
        all(
            feature = "clippy",
            not(any(
                feature = "audit",
                feature = "deny",
                feature = "outdated",
                feature = "udeps"
            ))
        ),
        allow(unused)
    )]
    lockfile: Lockfile,
}

impl Converter {
    /// Create a new converter
    ///
    /// # Errors
    /// May fail when trying to read and parse the 'Cargo.lock' file.
    #[inline]
    pub fn try_new() -> Result<Self> {
        let path = PathBuf::from("Cargo.lock");
        let lockfile = Lockfile::try_from(path.as_path())?;
        Ok(Self { lockfile })
    }

    fn report<'c, F>(&'c self, options: &Command) -> Result<F::Report>
    where
        F: FromIssues<'c>,
    {
        let mut issues: Box<dyn Iterator<Item = IssueType<'_>>> = Box::new(std::iter::empty());
        #[cfg(feature = "audit")]
        if options.audit {
            issues = Box::new(issues.chain(
                audit::Audit::try_new(&options.audit_path, &self.lockfile)?.map(IssueType::Audit),
            ));
        }
        #[cfg(feature = "clippy")]
        if options.clippy {
            issues = Box::new(
                issues.chain(
                    clippy::Clippy::try_new(&options.clippy_path)?
                        .map(Box::new)
                        .map(IssueType::Clippy),
                ),
            );
        }
        #[cfg(feature = "deny")]
        if options.deny {
            issues = Box::new(issues.chain(
                deny::Deny::try_new(&options.deny_path, &self.lockfile)?.map(IssueType::Deny),
            ));
        }
        #[cfg(feature = "outdated")]
        if options.outdated {
            issues = Box::new(
                issues.chain(
                    outdated::Outdated::try_new(&options.outdated_path, &self.lockfile)?
                        .map(IssueType::Outdated),
                ),
            );
        }
        #[cfg(feature = "udeps")]
        if options.udeps {
            issues = Box::new(issues.chain(
                udeps::Udeps::try_new(&options.udeps_path, &self.lockfile)?.map(IssueType::Udeps),
            ));
        }
        Ok(F::from_issues(issues))
    }

    /// Transform all the issues into Sonar compatible format
    ///
    /// # Errors
    /// If any input file is having a problem to read or parse, this operation will result in an error.
    #[cfg(feature = "sonar")]
    #[inline]
    pub fn sonarize(&self, options: &Command) -> Result<sonar::Issues> {
        self.report::<Sonar>(options)
    }
    /// Transform all the issues into Codeclimate compatible format
    ///
    /// # Errors
    /// If any input file is having a problem to read or parse, this operation will result in an error.
    #[cfg(feature = "codeclimate")]
    #[inline]
    pub fn codeclimatize(&self, options: &Command) -> Result<codeclimate::Issues> {
        self.report::<CodeClimate>(options)
    }
}
