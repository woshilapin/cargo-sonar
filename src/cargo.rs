use crate::TextRange;
use eyre::Context as _;
use std::{
    collections::BTreeMap,
    fs::File,
    io::{BufRead as _, BufReader},
    path::{Path, PathBuf},
};
use tracing::error;

#[derive(Debug)]
pub struct PackageRange {
    pub range: TextRange,
    pub name_range: TextRange,
    pub version_range: TextRange,
}

#[derive(Debug)]
pub struct Lockfile {
    pub lockfile_path: PathBuf,
    pub dependencies: BTreeMap<String, PackageRange>,
}

impl Lockfile {
    #[must_use]
    #[inline]
    pub fn dependency_range(&self, name: &str) -> TextRange {
        self.dependencies.get(name).map_or_else(
            || {
                error!("failed to find a corresponding text range for dependency '{}'. This is probably a bug in `cargo-sonar`, if you can please report an issue at https://gitlab.com/woshilapin/cargo-sonar/-/issues, we'd be happy to try and fix it.", name);
                TextRange::new((1, 0), (1, 0))
            },
            |ranges| ranges.range,
        )
    }
}

#[derive(Debug)]
enum StateParser {
    // on '[[package]]'
    Package {
        start_line: usize,
    },
    // on 'name = "package-name"'
    Name {
        start_line: usize,
        package_name: String,
        name_range: TextRange,
    },
    // on 'version = "1.2.3"'
    Version {
        start_line: usize,
        package_name: String,
        name_range: TextRange,
        version_range: TextRange,
    },
    // on empty line
    End,
}

impl TryFrom<&Path> for Lockfile {
    type Error = eyre::Error;

    #[inline]
    fn try_from(lockfile_path: &Path) -> Result<Self, Self::Error> {
        let lockfile_path = lockfile_path
            .canonicalize()
            .with_context(|| format!("failed to canonicalize '{lockfile_path:?}"))?;
        let mut dependencies = BTreeMap::new();
        let file = File::open(&lockfile_path)
            .with_context(|| format!("failed to open '{lockfile_path:?}"))?;
        let reader = BufReader::new(file);
        let mut state = StateParser::End;
        for (line_num, line) in reader
            .lines()
            // Add an empty line at the end of the file so the last package can be parsed with a tailing empty line too
            .chain(std::iter::once(Ok(String::new())))
            .enumerate()
        {
            let line_num = line_num.saturating_add(1);
            let line = line?;
            let create_range = |line: &str| {
                let start_column = line.find('"').unwrap_or(0);
                let end_column = line
                    .rfind('"')
                    .unwrap_or_else(|| line.len().saturating_sub(1));
                TextRange::new((line_num, start_column), (line_num, end_column))
            };
            match state {
                StateParser::End if line == "[[package]]" => {
                    state = StateParser::Package {
                        start_line: line_num,
                    };
                }
                StateParser::Package { start_line } if line.starts_with("name = ") => {
                    let name_range = create_range(&line);
                    state = StateParser::Name {
                        start_line,
                        package_name: line.replace("name = ", "").replace('"', ""),
                        name_range,
                    };
                }
                StateParser::Name {
                    start_line,
                    package_name,
                    name_range,
                } if line.starts_with("version = ") => {
                    let version_range = create_range(&line);
                    state = StateParser::Version {
                        start_line,
                        package_name,
                        name_range,
                        version_range,
                    };
                }
                StateParser::Version {
                    start_line,
                    package_name,
                    name_range,
                    version_range,
                } if line.is_empty() => {
                    let range = TextRange::new((start_line, 0), (line_num, 0));
                    let package_range = PackageRange {
                        range,
                        name_range,
                        version_range,
                    };
                    dependencies.insert(package_name, package_range);
                    state = StateParser::End;
                }
                s => state = s,
            }
        }
        let lockfile = Self {
            lockfile_path,
            dependencies,
        };
        Ok(lockfile)
    }
}

#[cfg(test)]
mod tests {
    use crate::Lockfile;
    use std::io::Write as _;
    use test_log::test;

    #[test]
    fn cargo_lock_parsing() {
        let lock = r#"
version = 3

[[package]]
name = "addr2line"
version = "0.20.0"
source = "registry+https://github.com/rust-lang/crates.io-index"
checksum = "f4fa78e18c64fce05e902adecd7a5eed15a5e0a3439f7b0e169f0252214865e3"
dependencies = [
"gimli",
]
        "#;
        let mut cargo_lock = tempfile::NamedTempFile::new().unwrap();
        write!(cargo_lock, "{}", lock).unwrap();

        let lockfile = Lockfile::try_from(cargo_lock.path()).unwrap();
        let text_range = lockfile.dependency_range("addr2line");
        assert_eq!(text_range.start.line, 4);
        assert_eq!(text_range.start.column, 0);
        assert_eq!(text_range.end.line, 13);
        assert_eq!(text_range.end.column, 0);
    }
}
