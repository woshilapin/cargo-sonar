use clap::CommandFactory;
use clap_complete::{generator::generate, shells};
use std::path::PathBuf;

#[cfg(any(
    feature = "audit",
    feature = "clippy",
    feature = "deny",
    feature = "outdated",
    feature = "udeps"
))]
#[derive(Debug, Clone, Copy, PartialEq, clap::ValueEnum, strum::EnumString)]
#[non_exhaustive]
pub enum Issue {
    #[cfg(feature = "audit")]
    #[strum(serialize = "audit")]
    Audit,
    #[cfg(feature = "clippy")]
    #[strum(serialize = "clippy")]
    Clippy,
    #[cfg(feature = "deny")]
    #[strum(serialize = "deny")]
    Deny,
    #[cfg(feature = "outdated")]
    #[strum(serialize = "outdated")]
    Outdated,
    #[cfg(feature = "udeps")]
    #[strum(serialize = "udeps")]
    Udeps,
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, clap::ValueEnum)]
#[non_exhaustive]
// ALLOW: 'SHell' is incorrectly detected as code that must be wrapped into backticks
#[allow(clippy::doc_markdown)]
pub enum Shell {
    /// Bourne Again SHell (bash)
    Bash,
    /// Elvish shell
    Elvish,
    /// Friendly Interactive SHell (fish)
    Fish,
    /// Nushell
    Nushell,
    /// PowerShell
    // ALLOW: 'PowerShell' is the name that does ends with 'Shell'
    #[allow(clippy::enum_variant_names)]
    PowerShell,
    /// Z SHell (zsh)
    Zsh,
}

// ALLOW: This `struct` is the user external API, and is expected to provide all
// the different supported format as different boolean options.
#[allow(clippy::struct_excessive_bools)]
#[derive(Debug, clap::Parser)]
#[command(version, author)]
pub struct Command {
    /// Enable parsing of the 'cargo audit' report
    #[cfg(feature = "audit")]
    #[arg(long)]
    pub audit: bool,
    /// Path of the 'cargo audit' report
    #[cfg(feature = "audit")]
    #[arg(long, default_value = "audit.json")]
    pub audit_path: PathBuf,
    /// Enable parsing of the 'cargo clippy' report
    #[cfg(feature = "clippy")]
    #[arg(long)]
    pub clippy: bool,
    /// Path of the 'cargo clippy' report
    #[cfg(feature = "clippy")]
    #[arg(long, default_value = "clippy.json")]
    pub clippy_path: PathBuf,
    /// Enable parsing of the 'cargo deny' report
    #[cfg(feature = "deny")]
    #[arg(long)]
    pub deny: bool,
    /// Path of the 'cargo deny' report
    #[cfg(feature = "deny")]
    #[arg(long, default_value = "deny.json")]
    pub deny_path: PathBuf,
    /// Enable parsing of the 'cargo outdated' report
    #[cfg(feature = "outdated")]
    #[arg(long)]
    pub outdated: bool,
    /// Path of the 'cargo outdated' report
    #[cfg(feature = "outdated")]
    #[arg(long, default_value = "outdated.json")]
    pub outdated_path: PathBuf,
    /// Enable parsing of the 'cargo udeps' report
    #[cfg(feature = "udeps")]
    #[arg(long)]
    pub udeps: bool,
    /// Path of the 'cargo udeps' report
    #[cfg(feature = "udeps")]
    #[arg(long, default_value = "udeps.json")]
    pub udeps_path: PathBuf,
    /// Path to the generated sonar report
    #[arg(long, default_value = "sonar-issues.json")]
    pub sonar_path: PathBuf,
    /// Path to the generated codeclimate report
    #[arg(long, default_value = "codeclimate.json")]
    pub codeclimate_path: PathBuf,
    /// Generate completion rules for some shells
    #[arg(long, value_enum)]
    pub completion: Option<Shell>,
}

impl Command {
    #[inline]
    pub fn generate_completion(&self) {
        if let Some(ref shell) = self.completion {
            let mut app = Self::command();
            let mut fd = std::io::stdout();
            match *shell {
                Shell::Bash => generate(shells::Bash, &mut app, "cargo-sonar", &mut fd),
                Shell::Elvish => generate(shells::Elvish, &mut app, "cargo-sonar", &mut fd),
                Shell::Fish => generate(shells::Fish, &mut app, "cargo-sonar", &mut fd),
                Shell::Nushell => generate(
                    clap_complete_nushell::Nushell,
                    &mut app,
                    "cargo-sonar",
                    &mut fd,
                ),
                Shell::PowerShell => generate(shells::PowerShell, &mut app, "cargo-sonar", &mut fd),
                Shell::Zsh => generate(shells::Zsh, &mut app, "cargo-sonar", &mut fd),
            }
            std::process::exit(0);
        }
    }
}
