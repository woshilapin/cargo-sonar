use assert_cmd::Command;
use std::{fmt::Display, path::PathBuf};

const NB_ISSUES: usize = 26;

#[derive(Debug, Clone, Copy)]
enum Binary {
    Sonar,
    Codeclimate,
}

impl Display for Binary {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            Binary::Sonar => write!(f, "cargo-sonar"),
            Binary::Codeclimate => write!(f, "cargo-codeclimate"),
        }
    }
}

fn run_binary(binary: Binary) -> tempfile::NamedTempFile {
    let output_tempfile = tempfile::NamedTempFile::new().unwrap();
    let output_path = output_tempfile.path();
    let output_arg = match binary {
        Binary::Sonar => "--sonar-path",
        Binary::Codeclimate => "--codeclimate-path",
    };
    Command::cargo_bin(binary.to_string())
        .unwrap()
        .current_dir(
            PathBuf::from(std::env::var("CARGO_MANIFEST_DIR").unwrap())
                .join("tests")
                .join("fixtures"),
        )
        .arg("--audit")
        .arg("--clippy")
        .arg("--deny")
        .arg("--outdated")
        .arg("--udeps")
        .arg(output_arg)
        .arg(output_path)
        .assert()
        .success();
    output_tempfile
}

#[test]
fn cargo_sonar() {
    let output_path = run_binary(Binary::Sonar);
    let issues: cargo_sonar::sonar::Issues =
        serde_json::from_reader(std::fs::File::open(output_path).unwrap()).unwrap();
    assert_eq!(issues.len(), NB_ISSUES);
}

#[test]
fn cargo_codeclimate() {
    let output_path = run_binary(Binary::Codeclimate);
    let issues: cargo_sonar::codeclimate::Issues =
        serde_json::from_reader(std::fs::File::open(output_path).unwrap()).unwrap();
    assert_eq!(issues.len(), NB_ISSUES);
}
