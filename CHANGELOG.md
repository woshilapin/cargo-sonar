# Changelog
All notable changes to this project will be documented in this file. See [conventional commits](https://www.conventionalcommits.org/) for commit guidelines.

- - -
## [1.3.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/2edd13cf56336af585a4efd2f94455dd41832eca..1.3.0) - 2025-01-07
#### Bug Fixes
- remove minor clippy lints - ([19338b6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/19338b6b86311fc9f585e34507d9a4c427670e11)) - Jean SIMARD
- cargo-codeclimate binary was failing to run - ([2edd13c](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2edd13cf56336af585a4efd2f94455dd41832eca)) - Jean SIMARD
#### Documentation
- add badges to the README - ([1ec16b6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1ec16b6a0c30571f32306928ec8b9f625f6b0f20)) - Jean SIMARD
- update installation instructions - ([60541ef](https://gitlab.com/woshilapin/cargo-sonar/-/commit/60541efd749db3851628fe44985f08d6e3b81a25)) - Jean SIMARD
#### Features
- bump to 'thiserror:2' - ([7828458](https://gitlab.com/woshilapin/cargo-sonar/-/commit/78284583a34c5ea677e29b59db543c0e42e2c0cb)) - Jean SIMARD
- upgrade dependencies - ([f2cdb7f](https://gitlab.com/woshilapin/cargo-sonar/-/commit/f2cdb7fa31eecdf9db3e4f4ff29747c00c4b5002)) - Jean SIMARD

- - -

## [1.2.1](https://gitlab.com/woshilapin/cargo-sonar/-/compare/af8ea4474d98f60e433f92398608090b82df31f3..1.2.1) - 2025-01-07
#### Bug Fixes
- produce non-empty ranges - ([09b1603](https://gitlab.com/woshilapin/cargo-sonar/-/commit/09b160340b124ce8083d196bab1079160e86b3ec)) - Jean SIMARD
#### Build system
- move 'clippy' lints to 'Cargo.toml' - ([af8ea44](https://gitlab.com/woshilapin/cargo-sonar/-/commit/af8ea4474d98f60e433f92398608090b82df31f3)) - Jean SIMARD
#### Continuous Integration
- replace 'cargo-tomlfmt' with 'taplo' - ([cb6bd32](https://gitlab.com/woshilapin/cargo-sonar/-/commit/cb6bd3281ee358acdaba4c53a0853b8461cb9f62)) - Jean SIMARD
- do not depend on 'SONAR_TOKEN' for launching 'cargo-sonar' - ([91cc530](https://gitlab.com/woshilapin/cargo-sonar/-/commit/91cc5309adf5206e64f0234a76596e12062b258c)) - Jean SIMARD
#### Miscellaneous Chores
- **(gitlab-ci)** remove duplicate pipelines - ([2c045d9](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2c045d93a8142936502eee1fa38fe1408f828378)) - Vasileios Anagnostopoulos
- update dependencies - ([3dd80e2](https://gitlab.com/woshilapin/cargo-sonar/-/commit/3dd80e27bd0b3713458cec4298d547e49c2a9550)) - Vasileios Anagnostopoulos
- add guix 'manifest.scm' - ([38e0121](https://gitlab.com/woshilapin/cargo-sonar/-/commit/38e0121f4eee760764ee1c5ca339ea50d29b2077)) - Jean SIMARD
- remove some needless lifetimes - ([9fc0b93](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9fc0b937e6a94c5fcc5632697caaafedf35511b4)) - Jean SIMARD
- add 'clippy.toml' to ignore panic in tests - ([a9ed24b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/a9ed24b28a174111c0c453b961040f14ef858ca1)) - Jean SIMARD
- fix 'clippy:1.83' warnings - ([ce5aa92](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ce5aa92de947ebca0673d8fa352afe7cc6c45668)) - Jean SIMARD

- - -

## [1.2.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/1.1.0..1.2.0) - 2023-11-06
#### Features
- add shell completion generation - ([22f275e](https://gitlab.com/woshilapin/cargo-sonar/-/commit/22f275e5ab72a5cd7bec1c391b7887e884a3d5a2)) - Jean SIMARD

- - -

## [1.1.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/1.0.0..1.1.0) - 2023-11-04
#### Features
- support the new Sonar issue format - ([cdac849](https://gitlab.com/woshilapin/cargo-sonar/-/commit/cdac8492f8bf48ee4facd76b888238e75b77eab2)) - Jean SIMARD

- - -

## [1.0.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.20.2..1.0.0) - 2023-09-13
#### Build system
- add 'cargo-codeclimate' in the Container image - ([2749550](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2749550ca27f0e4b05168c0a4f789ca490ca2ab6)) - Jean SIMARD
#### Continuous Integration
- add 'cargo-hack' check - ([42e95ec](https://gitlab.com/woshilapin/cargo-sonar/-/commit/42e95ec8c7f2aa9d95e63f65eec97a6f3c8e7c51)) - Jean SIMARD
#### Documentation
- update README with 'cargo-codeclimate' - ([207e3e8](https://gitlab.com/woshilapin/cargo-sonar/-/commit/207e3e82f7c40284c0bd9ef9de972adcd9db866d)) - Jean SIMARD
#### Features
- bump all dependencies - ([7881b0d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7881b0d0476149de1ee3f2ca2bdde2a2ddf70daf)) - Jean SIMARD
- new external CLI API - ([c9c2178](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c9c2178543dfe0c0ae26e0b3911acb6dea858a26)) - Jean SIMARD
- improve error message when lockfile not found - ([6b3313b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6b3313b50c16a7815cffef61506649faf2a59460)) - Jean SIMARD
- make all output's structs 'impl Deserialize' - ([2e15315](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2e15315ee380790caacd219c71b602c429a48c62)) - Jean SIMARD
- remove unused enum and API option - ([f6ae385](https://gitlab.com/woshilapin/cargo-sonar/-/commit/f6ae3857d01a353e763ffac01bf6915fb0909958)) - Jean SIMARD
- remove coverage support - ([2d1437e](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2d1437ea47bcb4c125cd5ab47548a6875cc0181a)) - Jean SIMARD
- add 'must_use' on 'dependency_range' - ([c79513d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c79513d43e2e2442158dfae2cf535a8687946b6a)) - Jean SIMARD
#### Tests
- add test for the 'lockfile' parsing - ([49e1eac](https://gitlab.com/woshilapin/cargo-sonar/-/commit/49e1eac139e6118b630f73581c4ede75f6b862f3)) - Jean SIMARD

- - -

## [0.20.2](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.20.1..0.20.2) - 2023-09-11
#### Bug Fixes
- parsing correctly 'cargo-outdated' output for workspaces - ([1640712](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1640712a8a7a213b645c1a813cd324f450a52a4c)) - Jean SIMARD
#### Miscellaneous Chores
- remove unused file (artefact from the refactoring) - ([0b4d47b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/0b4d47bc625665c0a3c3cf99732a36620fccfbe2)) - Jean SIMARD
#### Style
- fix some clippy warnings - ([6ba99a7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6ba99a72d3b649d2504b3b2a2839145d6b966742)) - Jean SIMARD

- - -

## [0.20.1](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.20.0..0.20.1) - 2023-09-09
#### Bug Fixes
- make all combinations of features work - ([f7f06d2](https://gitlab.com/woshilapin/cargo-sonar/-/commit/f7f06d293b269ad36bc38cc0ec95cc0c0288d94a)) - Jean SIMARD
- '--issues' is working as expected - ([32091e8](https://gitlab.com/woshilapin/cargo-sonar/-/commit/32091e841a4d8bf012d5702fc31e9ceec29ca289)) - Jean SIMARD
#### Continuous Integration
- add login and username for auto-commit - ([d75d214](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d75d2144701523d23c593a17ed94600c82274afd)) - Jean SIMARD
- add a gitlab token for pushing back to git repository - ([aaec57e](https://gitlab.com/woshilapin/cargo-sonar/-/commit/aaec57e45226c5ecaa94770514bfa4d6e8aff330)) - Jean SIMARD
- define a base image for 'build' - ([d40e7a6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d40e7a675a41381a70a24728aabc4be398a89a0a)) - Jean SIMARD
- pin minimal versions - ([cbe2551](https://gitlab.com/woshilapin/cargo-sonar/-/commit/cbe2551e5231165b072d09da5d28b6c2ecce1031)) - Jean SIMARD
- use gitlab CI/CD components - ([5174c9b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5174c9b1e175b39ff9fb2b69fae86cc3a04d7588)) - Jean SIMARD
- update to 'cargo-udeps:0.1.42' - ([18dd5b9](https://gitlab.com/woshilapin/cargo-sonar/-/commit/18dd5b9ac6b815b95ba16059a93a8faa3942e668)) - Jean SIMARD
#### Documentation
- add automatic feature's documentation - ([3906bd3](https://gitlab.com/woshilapin/cargo-sonar/-/commit/3906bd3ec400275cf3cb12759e7a51f6293feb40)) - Jean SIMARD

- - -

## [0.20.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.19.0..0.20.0) - 2023-08-10
#### Features
- add codeclimate issue generator - ([0d761b4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/0d761b479da5593aaca67c5411ef01828f852791)) - Jean SIMARD
#### Miscellaneous Chores
- **(version)** 0.20.0 - ([23784da](https://gitlab.com/woshilapin/cargo-sonar/-/commit/23784da50a9d80461334141fcb05c0b52a218f23)) - gitlab-ci

- - -

## [0.20.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.19.0..0.20.0) - 2023-08-10
#### Features
- add codeclimate issue generator - ([0d761b4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/0d761b479da5593aaca67c5411ef01828f852791)) - Jean SIMARD

- - -

## [0.19.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.18.0..0.19.0) - 2023-08-01
#### Continuous Integration
- add release for 'aarch64-unknown-linux-musl' - ([c0eb3db](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c0eb3dbd5c735f7b7a2da4128a69b914ac8572b2)) - Jean SIMARD
#### Style
- fix clippy warnings - ([d435100](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d435100919b2707e9f5062638fb3014b8c8612b5)) - Jean SIMARD

- - -

## [0.18.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.17.0..0.18.0) - 2023-07-12
#### Continuous Integration
- push OCI images to Gitlab registry - ([8476438](https://gitlab.com/woshilapin/cargo-sonar/-/commit/847643875041eb3d524094e56c8d72e0ec644d27)) - Jean SIMARD
#### Documentation
- update installation instructions - ([a824348](https://gitlab.com/woshilapin/cargo-sonar/-/commit/a824348149f2075570dd8078ae883750b03afaa8)) - Jean SIMARD

- - -

## [0.17.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.16.0..0.17.0) - 2023-07-11
#### Bug Fixes
- release target are now correct - ([8b49d2d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/8b49d2d2861982064dde81b4c3a3e2b0f9322037)) - Jean SIMARD
#### Continuous Integration
- do not 'build' on merge requests - ([1a6ec84](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1a6ec84612b5191f62d74179d14afc98563c544e)) - Jean SIMARD
#### Features
- configure correctly for 'cargo-binstall' - ([db96bd1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/db96bd1fa61b77b881a4e9ae7afb8f04193b419d)) - Jean SIMARD
#### Tests
- add test for 'udeps' - ([05b47cb](https://gitlab.com/woshilapin/cargo-sonar/-/commit/05b47cbae6940df5481c739802610dc2b8c69f1e)) - Jean SIMARD
- configuration for codecov - ([d140fde](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d140fdee9932e40b31ed998a004be54efb14043f)) - Jean SIMARD
- fix cobertura report for Gitlab's use - ([8947aed](https://gitlab.com/woshilapin/cargo-sonar/-/commit/8947aedaca41d6b7f2292669371e7c058813c080)) - Jean SIMARD
- use 'codecov' functionality from 'cargo-llvm-cov' - ([b52b231](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b52b23141e35a6c0af790c71358232a07ec34978)) - Jean SIMARD

- - -

## [0.16.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.15.0..0.16.0) - 2023-07-06
#### Build system
- remove useless dependencies - ([4048566](https://gitlab.com/woshilapin/cargo-sonar/-/commit/404856615bc3539eca78734ce71e861656abf709)) - Jean SIMARD
#### Continuous Integration
- create a release on 'tag' - ([2be2575](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2be25752002ceef68d82d069e530c87c5f6141bb)) - Jean SIMARD
- add coverage reporting for Gitlab - ([924eb88](https://gitlab.com/woshilapin/cargo-sonar/-/commit/924eb88f4802eee63dad27ed1665576ff62f66df)) - Jean SIMARD
- add coverage generation and push to CodeCov - ([41b1e20](https://gitlab.com/woshilapin/cargo-sonar/-/commit/41b1e20ae5d0463a20542884982efa7f42a21a02)) - Jean SIMARD
#### Documentation
- update documentation for releases - ([e3a25b6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/e3a25b66c3729554777be63adbbc37e007632366)) - Jean SIMARD
#### Refactoring
- use 'strum' with 'derive' feature - ([218625d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/218625d3a1c5f0232db4896b0e642628a8980987)) - Jean SIMARD
- use 'strum' for enums - ([a54169f](https://gitlab.com/woshilapin/cargo-sonar/-/commit/a54169f12023cc42b42d070b80aad1f815ab7deb)) - Jean SIMARD
#### Tests
- add tests for 'clippy' conversion - ([b542cec](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b542cec40e2cb0b720456614fdf8a37f13c1a3e0)) - Jean SIMARD

- - -

## [0.15.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.14.1..0.15.0) - 2023-06-14
#### Bug Fixes
- bump to 'typos:1.15.0' - ([9d9c83b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9d9c83baadeb021d56a956f779699497f47d111e)) - Jean SIMARD
#### Continuous Integration
- bump to 'cargo-audit:0.17.6' - ([ace52d7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ace52d7cf62e8366b6a907b133a41e1f0b9ac64c)) - Jean SIMARD
- bump to 'cargo-deny:0.13.9' - ([363e446](https://gitlab.com/woshilapin/cargo-sonar/-/commit/363e446a98168903d73a7efdca506c6229f79bbd)) - Jean SIMARD
- bump to 'cargo-outdated:0.11.2' - ([df866f8](https://gitlab.com/woshilapin/cargo-sonar/-/commit/df866f8ec409710eb9188d3c4c2e74a45f7fb583)) - Jean SIMARD
- bump to 'cargo-udeps:0.1.40' - ([a399873](https://gitlab.com/woshilapin/cargo-sonar/-/commit/a399873ac5968ba6d16ea6b2859d0b54e574acdb)) - Jean SIMARD
#### Features
- bump to 'clap:4.3.3' - ([f49a520](https://gitlab.com/woshilapin/cargo-sonar/-/commit/f49a52022a0a4480088a78eb4673f93c75012158)) - Jean SIMARD
- cargo update - ([207a69a](https://gitlab.com/woshilapin/cargo-sonar/-/commit/207a69af2313130d82a29eac1f6c101121fdb2f6)) - Jean SIMARD
- bump to 'cargo_metadata:0.15.4' - ([c7c419b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c7c419b9d02fc3a15fe9385d62b5e483c584c0f1)) - Jean SIMARD
- bump to 'color-eyre:0.6.2' - ([2bf783b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2bf783be91f196e2fe52792411968a054f5441b6)) - Jean SIMARD
- bump to 'rustsec:0.27.0' - ([1049263](https://gitlab.com/woshilapin/cargo-sonar/-/commit/10492631285cfe5eeb1f4902033dbd9858579c31)) - Jean SIMARD
- bump to 'quick-xml:0.29.0' - ([6a25470](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6a2547005b5f5f40511216e63aa78890881b5e03)) - Jean SIMARD
#### Style
- fix clippy warnings - ([9dc5f1f](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9dc5f1f8e1f3becde0083c6037bd30fadabd9109)) - Jean SIMARD

- - -

## [0.14.1](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.14.0..0.14.1) - 2022-10-21
#### Bug Fixes
- all features are optional - ([265acd4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/265acd49ee09c19738f56e9f04b16643f4111a26)) - Jean SIMARD

- - -

## [0.14.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.13.0..0.14.0) - 2022-10-20
#### Continuous Integration
- attempt to fix weird error on sonar CI - ([2025f2d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2025f2d04221c6d932954a4bf980c1ca14916046)) - Jean SIMARD
#### Features
- define working directory and add documentation - ([9b4d57b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9b4d57ba8aca11628934a2cc4e6083ac3e394af0)) - Jean SIMARD

- - -

## [0.13.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.12.1..0.13.0) - 2022-10-20
#### Features
- define an entrypoint for the container - ([0ef5ade](https://gitlab.com/woshilapin/cargo-sonar/-/commit/0ef5ade1af5b50aa9cd71a06e52b6fd835c1f199)) - Jean SIMARD

- - -

## [0.12.1](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.12.0..0.12.1) - 2022-08-26
#### Bug Fixes
- parsing of audit files works again - ([d1f1561](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d1f156106a352a6ac12730c803c084b7b5512ab3)) - Kateřina Churanová

- - -

## [0.12.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.11.0..0.12.0) - 2022-07-19
#### Bug Fixes
- remove a 'clippy::pattern_type_mismatch' - ([ab0b226](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ab0b226863179c45b22f3f758e35694ff2a9835c)) - Jean SIMARD
#### Continuous Integration
- update to 'cargo-deny:0.11.2' - ([5b89b69](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5b89b696f9b913692e429324c4196764410be430)) - Jean SIMARD
- update to 'typos-cli:1.4.0' - ([6664a57](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6664a5796c5c2b8b838af059c008f4703501f408)) - Jean SIMARD
#### Features
- upgrade all dependencies - ([7efd5ae](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7efd5ae2fd2a672b2ce9ce80f35759fdb824b995)) - Jean SIMARD
- upgrade to 'cocogitto:5.1.0' - ([1b5996b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1b5996b9cd19ab12e48106194278a75590be7796)) - Jean SIMARD
- upgrade to 'cargo-outdated:0.11.1' - ([7571f15](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7571f155928692cac3264503d6db30d28d09339e)) - Jean SIMARD
- upgrade to 'cargo-deny:0.12.1' - ([23b97ac](https://gitlab.com/woshilapin/cargo-sonar/-/commit/23b97ac33e80af3f6c673c978b71a255c7205d09)) - Jean SIMARD
- upgrade to 'cargo-audit:0.17.0' - ([aad5e56](https://gitlab.com/woshilapin/cargo-sonar/-/commit/aad5e566258a8b8e0c68c22ab16c8f56767adb26)) - Jean SIMARD
- upgrade to 'tarpaulin:0.20.1' - ([6919993](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6919993449ae01008eae79f8b1bc248c248d508b)) - Jean SIMARD
- upgrade to 'typos:1.10.2' - ([09dfd51](https://gitlab.com/woshilapin/cargo-sonar/-/commit/09dfd51b31ee9e9d2fe708f39ae8edd6caeba2dd)) - Jean SIMARD
- upgrade to 'cargo-udeps:0.1.30' - ([5191824](https://gitlab.com/woshilapin/cargo-sonar/-/commit/51918243420b83c2ce142bc0a45639777590a791)) - Jean SIMARD

- - -

## [0.11.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.10.2..0.11.0) - 2022-02-04
#### Bug Fixes
- calling 'cargo sonar' does not fail anymore - ([9b57001](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9b570016bbd5f9d79ad3190191be0c9b25b71055)) - Jean SIMARD
#### Features
- update all dependencies - ([9e6fbfb](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9e6fbfb50b432d77067f0dbcfc87ce3154065c7d)) - Jean SIMARD
- - -

## [0.10.2](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.10.1..0.10.2) - 2022-02-04
#### Bug Fixes
- allow running without having --coverage specified - ([4779432](https://gitlab.com/woshilapin/cargo-sonar/-/commit/4779432697bff7432420f197bdad330a54484203)) - Kristof Mattei
#### Continuous Integration
- update to 'cargo-deny:0.11.1' - ([5887816](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5887816360e63c6c117a38bbce77334a5e95e07d)) - Jean SIMARD
- update to 'typos-cli:1.3.9' - ([852f519](https://gitlab.com/woshilapin/cargo-sonar/-/commit/852f5198775316e717a5b4ebd63fe5c0e681b142)) - Jean SIMARD
- update to 'typos-cli:1.3.7' - ([2845d6d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2845d6d7199db495ec894b1a8f23815655ea69bd)) - Jean SIMARD
- update to 'typos-cli:1.3.6' - ([fac1fdc](https://gitlab.com/woshilapin/cargo-sonar/-/commit/fac1fdc00fb1d34614d69455976585f695e61d81)) - Jean SIMARD
- update to 'cargo-udeps:0.1.26' - ([8788752](https://gitlab.com/woshilapin/cargo-sonar/-/commit/8788752d63ff084100523549e6570c0cb97e914f)) - Jean SIMARD
- update to 'tarpaulin:0.19.1' - ([6617014](https://gitlab.com/woshilapin/cargo-sonar/-/commit/66170148e9d0d48bd01e8d5d4220e2e4977f0b33)) - Jean SIMARD
- update to 'cargo-audit:0.16.0' - ([ec4a347](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ec4a3479eac7ac24daea3a542c4e647c3766700f)) - Jean SIMARD
- update to 'cocogitto:4.1.0' - ([703f9b4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/703f9b4f8fee3865622b7bc4d7b887ad66ab1891)) - Jean SIMARD
- update to 'typos-cli:1.3.4' - ([78cc114](https://gitlab.com/woshilapin/cargo-sonar/-/commit/78cc1149193682f5ea3dfdd4613638df95a45b58)) - Jean SIMARD
- do not run 'sonar' when 'SONAR_TOKEN' isn't available - ([b1567a5](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b1567a5af2774ef7448575fe0091f2de9e1607d2)) - Jean SIMARD
- - -

## [0.10.1](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.10.0..0.10.1) - 2021-12-15
#### Bug Fixes
- **(outdated)** handle multi-workspace projects - ([6f4a7eb](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6f4a7eb8bf425e0ecf8adc00afcc11a0dbfd2ca0)) - Thomas Loubiou
- - -

## [0.10.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.9.0..0.10.0) - 2021-12-15
#### Bug Fixes
- make 'clippy' much more strict and in-code - ([661e629](https://gitlab.com/woshilapin/cargo-sonar/-/commit/661e629e4be35903cf8bba81f5a16e2e48a9c7eb)) - Jean SIMARD
- remove some clippy warnings for enum and dead_code - ([6a1e2c7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6a1e2c7b29db4a33569d0924267cdd14552918ce)) - Jean SIMARD
#### Continuous Integration
- deactivate coverage publishing to sonarcloud - ([6d31821](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6d31821de53e3286b89396815c1d1c019a94b2bb)) - Jean SIMARD
- update to 'typos-cli:1.3.2' - ([b885a26](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b885a26bd9a17490586e74037063fea99d93ba75)) - Jean SIMARD
- define the default CI image correctly - ([860ed6b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/860ed6bf44b86f18f4616cbd7e7dede6eee189bb)) - Jean SIMARD
- add 'interruptible' jobs to not waste CI time - ([c0c1cb4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c0c1cb484f289ef03a5a369a75ffa7ec2b75c695)) - Jean SIMARD
- update to 'cargo-udeps:0.25.0' - ([3c9ff76](https://gitlab.com/woshilapin/cargo-sonar/-/commit/3c9ff7694e22df53b30afb27e519e51b3901adc8)) - Jean SIMARD
- update to 'cargo-deny:0.11.0' - ([b4980c1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b4980c1a3377228291ec971fcf0ec266ec375a25)) - Jean SIMARD
- update to 'cocogitto:4.0.1' - ([ef29db7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ef29db7d918eeed0868277c3aa46c1f459b1f0e1)) - Jean SIMARD
#### Features
- add coverage conversion for 'cargo-tarpaulin' - ([abf3b11](https://gitlab.com/woshilapin/cargo-sonar/-/commit/abf3b11c95dd4a270cf2049e28092f7f809ebde3)) - Jean SIMARD
- - -

## 0.9.0 - 2021-11-29


### Documentation

[0531a8](https://gitlab.com/woshilapin/cargo-sonar/-/commit/0531a8b4734a3bc70a9f9a0047e1aeabad43e6f8) - remove useless comment (already fixed) - Jean SIMARD

[02741b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/02741b995447cd7437f297b189ef20918c72095c) - fix a typo in a FIXME - Jean SIMARD


### Features

[c995e1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c995e1c67f59200245e34656faac422f440d8cef) - change CLI activation of options - Jean SIMARD

[1b2e78](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1b2e78fd2092fd108da393888cd788745d6983e0) - support for 'cargo-udeps' - Jean SIMARD

[af61ac](https://gitlab.com/woshilapin/cargo-sonar/-/commit/af61acbf2edcefbb349ec47ce391774be6876d1b) - update all dependencies - Jean SIMARD

[b19397](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b1939780449415f0351ae83624d4ae1c8178b9cc) - update 'tracing-subscriber' dependency - Jean SIMARD

[901906](https://gitlab.com/woshilapin/cargo-sonar/-/commit/90190694c1a37ff885058ea415af760c0873e4d4) - update 'thiserror' dependency - Jean SIMARD

[ab7d78](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ab7d7815bc5ee7f2c122934bdfc6018922a4aa19) - update 'serde_json' dependency - Jean SIMARD

[fdbeb1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/fdbeb18fdd708d430713353178bb6b2eb440eb52) - update 'rustsec' dependency - Jean SIMARD


### Continuous Integration

[18ccf6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/18ccf68354948d477e15454989723b495fbaebd2) - rely more on dependencies to speed up the CI - Jean SIMARD

[60d538](https://gitlab.com/woshilapin/cargo-sonar/-/commit/60d538938eab1ef007cf382e77d0f7690fd9f555) - update 'cargo-deny' dependency - Jean SIMARD


### Bug Fixes

[4770e6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/4770e6a4ef6cf0627441a7dd362cbefaf2ff0fae) - remove most clippy warnings - Jean SIMARD

[f1d7c0](https://gitlab.com/woshilapin/cargo-sonar/-/commit/f1d7c0cb445a8b91f24f28280b29968cba504445) - remove unused dependency - Jean SIMARD

[b9f049](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b9f049f97cb47eb71c28cc3b2e357be58557935a) - improve feature dependencies - Jean SIMARD

[9e2b67](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9e2b6747f5c1fe4b00fb4ab41c85d5baee717d98) - 'cargo-outdated' is not only-JSON when success - Jean SIMARD


### Miscellaneous Chores

[aad8f5](https://gitlab.com/woshilapin/cargo-sonar/-/commit/aad8f5d9ff707261cb254d892d3c799bde706593) - remove all json report from git - Jean SIMARD


- - -
## 0.8.1 - 2021-11-18


### Bug Fixes

[c1ca82](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c1ca82b7c5a0afa0a0a6cf03f237e50b6051e5b5) - do not fail because of end-of-line characters - Jean SIMARD


### Documentation

[38ad11](https://gitlab.com/woshilapin/cargo-sonar/-/commit/38ad11d88ad0623d949f871f6a4ccf96e263cf75) - improve documentation of 'cargo-deny' - Jean SIMARD

[97518f](https://gitlab.com/woshilapin/cargo-sonar/-/commit/97518f1d642d56d770e5577cfacda62202e9837f) - improve documentation of 'cargo-outdated' - Jean SIMARD


- - -
## 0.8.0 - 2021-11-17


### Refactoring

[7b7b92](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7b7b9277348ec792524d24e4792f5a0ecf708ac5) - change lifetimes' names - Jean SIMARD


### Documentation

[553dde](https://gitlab.com/woshilapin/cargo-sonar/-/commit/553dde03c253e722ece07603c2db898507a68f31) - update TODO list in the README.md - Jean SIMARD


### Bug Fixes

[272374](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2723740814354100decd1d1ffe239f56db80b7b3) - improve 'rule_id' to make them more unique - Jean SIMARD


### Features

[cae45f](https://gitlab.com/woshilapin/cargo-sonar/-/commit/cae45f8293bc68e35b699b6d7b1663fdc310dd98) - add support for 'cargo-outdated' - Jean SIMARD


### Continuous Integration

[91d3d6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/91d3d61be8358906e835840e11569299fae580b8) - add 'cargo-outdated' to the CI - Jean SIMARD


- - -
## 0.7.0 - 2021-11-17


### Features

[1c2755](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1c2755b4ee05b5450647dbc5f860d46e8ae3265e) - support for 'cargo-deny' - Jean SIMARD


### Continuous Integration

[868036](https://gitlab.com/woshilapin/cargo-sonar/-/commit/868036cc39eea83bac8fc465124fb85cb7f1dae8) - update to 'typos:1.3.1' - Jean SIMARD

[d44915](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d44915cb8f9b58d2083c979545de66c639c76136) - add 'cargo-deny' to the Sonar report - Jean SIMARD

[ae6651](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ae665139443b6880b84d0e12c7cf7929efe44e05) - upgrade to 'typos:1.3.0' - Jean SIMARD


- - -
## 0.6.0 - 2021-11-11


### Continuous Integration

[14f44c](https://gitlab.com/woshilapin/cargo-sonar/-/commit/14f44cc674e1112c9c78a7a9e70f67d63199d42c) - fix the extraction of 'cargo-tomlfmt' - Jean SIMARD

[53b4cb](https://gitlab.com/woshilapin/cargo-sonar/-/commit/53b4cbceed16adf5c7aa831f1031c7e337b364be) - do not exclude 'Cargo.lock' file, new version of 'typos' does it by default - Jean SIMARD

[f56859](https://gitlab.com/woshilapin/cargo-sonar/-/commit/f56859779d854216d8183672d69dc913f223f550) - fix the stage for sonar reporting - Jean SIMARD

[c4c319](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c4c319939cbd2b5a94e17db6e08e1377a552a7f7) - add 'cargo-tomlfmt' in the CI check - Jean SIMARD


### Documentation

[8e206b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/8e206b95fd0c5b2d36025b3e75e2f66ace43ef7f) - how to release manually - Jean SIMARD


### Features

[50e5bb](https://gitlab.com/woshilapin/cargo-sonar/-/commit/50e5bb9583d2f76047919efc8091eea6c64c330d) - use 'Cargo.lock' instead of 'Cargo.toml' for 'cargo-audit' - Jean SIMARD


### Refactoring

[98577d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/98577d5b65a000cd54083b98178c9f055dd2ffb2) - rename the 'Cargo.toml' parsing into 'Manifest' - Jean SIMARD


- - -
## 0.5.1 - 2021-11-10


### Bug Fixes

[26bfa7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/26bfa76929e68d5be8f3d3fb97fe8689cecf708a) - add useful error message when input report is missing - Jean SIMARD


### Continuous Integration

[86fbea](https://gitlab.com/woshilapin/cargo-sonar/-/commit/86fbea7dbaea2b9b2894698818378a6402bc515b) - remove the caching mechanism and use artifacts - Jean SIMARD

[c4a5bc](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c4a5bc43de58526673c68f702653b0a9c6c0fbb2) - fix caches - Jean SIMARD


- - -
## 0.5.0 - 2021-11-10


### Features

[52da0e](https://gitlab.com/woshilapin/cargo-sonar/-/commit/52da0e10fc6de13eb08ac531ce48ba48276884d1) - make the output file configurable - Jean SIMARD


### Continuous Integration

[349e1f](https://gitlab.com/woshilapin/cargo-sonar/-/commit/349e1f346f56699bf2d4f30587c0cfb2a4f386e0) - fix the caching mechanism - Jean SIMARD


- - -
## 0.4.0 - 2021-11-10


### Refactoring

[eb04ed](https://gitlab.com/woshilapin/cargo-sonar/-/commit/eb04edf45d81562db230b449e38ec3d7b56da77f) - rename some 'sonar' variables - Jean SIMARD


### Continuous Integration

[ca0caa](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ca0caa0cd5a1c74204ead6cf3939cf01ff745a8b) - 'git clean' before 'cog bump' - Jean SIMARD

[62384e](https://gitlab.com/woshilapin/cargo-sonar/-/commit/62384e18ec636be888a5b8e271f7128ea48494dc) - add some documentation and new stages - Jean SIMARD

[2f127b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2f127b54575661fb50761b7f922c6b119449ced7) - sonar scanner - Jean SIMARD

[98a164](https://gitlab.com/woshilapin/cargo-sonar/-/commit/98a164b2c019f59727ce0c25d2177e608bbd7f72) - fix check on conventional commit - Jean SIMARD

[04c2c3](https://gitlab.com/woshilapin/cargo-sonar/-/commit/04c2c3917fcf8e6d600003b74334950c8a33d8ad) - reorder steps in alphabetical order with comments - Jean SIMARD

[734490](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7344902d010b2d4b93227ed455caa3f10594f773) - add cargo-audit in CI - Jean SIMARD

[d2e6ac](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d2e6ac63fa33f2845a1da9a65a15f4689b8774f7) - add cache for clippy.json - Jean SIMARD


### Documentation

[5fdfb9](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5fdfb97a3dae8d9db57f89a3dececfdd368785fa) - add serious documentation in README - Jean SIMARD


### Build system

[98f539](https://gitlab.com/woshilapin/cargo-sonar/-/commit/98f5398277ea853e1c477ed97f7e846c97072485) - enable incremental compilation to use 'cargo-chef' - Jean SIMARD


### Features

[76374a](https://gitlab.com/woshilapin/cargo-sonar/-/commit/76374a5873e7300a017b20e940e80a67655e054b) - add CLI configuration - Jean SIMARD

[d9a2ff](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d9a2ffae9bf0e4984aa7dc47cf19f15e4d37fc37) - activate all features by default - Jean SIMARD

[12e58a](https://gitlab.com/woshilapin/cargo-sonar/-/commit/12e58a6d6afdd4ee689c244aadc8362f10c50aa6) - put 'audit' and 'clippy' functionalities behind features - Jean SIMARD

[ffe844](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ffe84499c25f23c1dd4dadb96f8d06705ab4aa24) - update all dependencies - Jean SIMARD


- - -
## 0.3.1 - 2021-11-04


### Bug Fixes

[fe2eb4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/fe2eb4d07418bcbe61040c4cd356b0967f2f801d) - binary access in the container image - Jean SIMARD


- - -
## 0.3.0 - 2021-11-03


### Miscellaneous Chores

[7650a2](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7650a2f68f40bf9bfd19295c8950390be30fe254) - add 'rust-toolchain.toml' - Jean SIMARD


### Build system

[651e7a](https://gitlab.com/woshilapin/cargo-sonar/-/commit/651e7affb2ecdf487906b72382839da590141bf8) - remove link optimization that causes problems everywhere - Jean SIMARD

[e6773d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/e6773d606cce946d1583abba5b3a7b1075808c84) - improve local build's time - Jean SIMARD


### Features

[277f68](https://gitlab.com/woshilapin/cargo-sonar/-/commit/277f68d51453ab5a0161de0dc4078f5f0accbefa) - do not depend on binaries anymore - Jean SIMARD


- - -
## 0.2.0 - 2021-10-27


### Miscellaneous Chores

[8fe82b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/8fe82b90549eae847114f3838cf2a0fedeb132fa) - add some FIXME/TODO with ideas for improvements - Jean SIMARD


### Features

[44412c](https://gitlab.com/woshilapin/cargo-sonar/-/commit/44412c8ee403fa02e7c1d597528e12cc96075fb1) - clippy is configured in 'pedantic' mode - Jean SIMARD

[61ef39](https://gitlab.com/woshilapin/cargo-sonar/-/commit/61ef39b52466a172c2c66567d482f3081419d9d2) - docker image now includes 'clippy' and 'audit' - Jean SIMARD


- - -
## 0.1.3 - 2021-10-26


### Continuous Integration

[1e04f7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1e04f7d943f59d6d70a89938e78188764c9eea3a) - update 'Cargo.lock' when tagging a new version - Jean SIMARD


### Bug Fixes

[77793c](https://gitlab.com/woshilapin/cargo-sonar/-/commit/77793c7392fdd9d674f51e8659b01849eb574078) - remove full-path of trait in the edition2021's prelude - Jean SIMARD


- - -
## 0.1.2 - 2021-10-26


### Tests

[c37008](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c37008edfb3fb9803dc75d0273976f18b024b43a) - add test for cargo parsing - Jean SIMARD


### Bug Fixes

[61b46b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/61b46b63c36bc1a5a385219ef1d79b6b2e54832f) - TryFrom/TryInto is in the prelude in edition2021 - Jean SIMARD


### Continuous Integration

[b316e7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b316e7e538aad2dddb393843e954b7e07fc8ca5b) - add 'cargo test' - Jean SIMARD

[8f6924](https://gitlab.com/woshilapin/cargo-sonar/-/commit/8f692444d1f9f8d1d1238b7cd1200de9a05e4639) - remove useless braces for variables - Jean SIMARD

[5137b5](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5137b5e9a8a1759a618825ff597d1389283603d3) - use full-path container image - Jean SIMARD


- - -
## 0.1.1 - 2021-10-25


### Continuous Integration

[1e71e9](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1e71e9ba3b4465bc53f5dbe824df6b3bb106ea6c) - automatically bump version of Cargo.toml - Jean SIMARD

[effd19](https://gitlab.com/woshilapin/cargo-sonar/-/commit/effd19bf84b8be1b0427a880c5a987a0089e2f38) - setting up git user is only useful for tagging - Jean SIMARD

[4c9fd1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/4c9fd19c1be0ec60fee208788d572a39a033bdb7) - automatic release of OCI image to Dockerhub - Jean SIMARD


### Miscellaneous Chores

[abef2d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/abef2d5e67f037990798af76bdf89fb4b7340867) - revert 0.1.1 - Jean SIMARD

[258609](https://gitlab.com/woshilapin/cargo-sonar/-/commit/25860929833ef4315850443aa1c492898f3ac11e) - 0.1.1 - gitlab-ci


### Bug Fixes

[9df357](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9df3575cd71aa4f78b02c0edd6bb995627c22c42) - need to update to 'cargo:0.57' to support edition 2021 - Jean SIMARD


### Documentation

[845c08](https://gitlab.com/woshilapin/cargo-sonar/-/commit/845c08ea330ac7183a056b7df3538b4478a0d5ad) - add TODO about documentation - Jean SIMARD

[7fc945](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7fc945e5baac6b5cd03beec7a088b6e2c2bf5d33) - open-source the project - Jean SIMARD


### Build system

[b1163c](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b1163c6bddfac794070790358e5af67227e964c6) - update to Rust 1.56.0 - Jean SIMARD


- - -
## 0.1.0 - 2021-10-25


### Documentation

[ffa03c](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ffa03cf9f907b90d43f0f95f24f9c9334e9e9327) - add a license file - Jean SIMARD

[4028b0](https://gitlab.com/woshilapin/cargo-sonar/-/commit/4028b00ea50925d45c6e7aae8345f4a711313254) - add a 'README.md' - Jean SIMARD


### Continuous Integration

[1b841b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1b841becd711ba2209f1fd18ef5579d24d1eb1c6) - automatic release - Jean SIMARD

[ef25d4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ef25d47c61e76144c74356c7ae24fe5874795c25) - add typos checking - Jean SIMARD

[27f42b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/27f42b2f8c4c0c6fe8f5798a655eacbf5980c301) - improve conventional commit check - Jean SIMARD

[d58a17](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d58a177135a31f906a3ade7249686101ca4c18a6) - add sonar-scanner configuration - Jean SIMARD

[6ae7e1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6ae7e1361896fb60214dedd07f2c64edda54d811) - add cocogitto check for conventional commits - Jean SIMARD

[004a35](https://gitlab.com/woshilapin/cargo-sonar/-/commit/004a35ab4b50cbf992db0b24f835e1f835e74907) - explode in multiple files - Jean SIMARD

[b4fac6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b4fac680ac29066bf0a8a907b5c5fb905c51c0df) - add clippy check - Jean SIMARD


### Features

[ebb1b4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ebb1b47c8fa67f23e8c533d50383b4f7d68ed8da) - convert audit reports into sonar reports - Jean SIMARD

[3f86d4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/3f86d4f0fd66e90b2a5c7e75d00b7f293b51e724) - add cargo parsing for text ranges - Jean SIMARD

[a8293e](https://gitlab.com/woshilapin/cargo-sonar/-/commit/a8293eeb56849bac32a112f5395f25f8fdcd0b98) - replace 'anyhow' with 'eyre' and 'color_eyre' - Jean SIMARD

[10aeb1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/10aeb1a871a7cf98d98fe9164a5bc2bf1d340b6b) - add error handling with anyhow - Jean SIMARD

[5ada39](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5ada39ec7a74ca663fb243824e2ab58fc6b59311) - add 'tracing' for logging - Jean SIMARD

[721099](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7210997e23e43e2b5e88d5e7736cb93d1c3d7085) - run 'cargo audit' without parsing yet - Jean SIMARD

[7d0bb9](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7d0bb95a1fe107c6fbdfe137d67a6212eff87296) - stream cargo clippy result to produce output - Jean SIMARD


### Miscellaneous Chores

[3d71bc](https://gitlab.com/woshilapin/cargo-sonar/-/commit/3d71bc5f5a360dae836190b7b1630312ed62bb1a) - improve metadata in Cargo.toml - Jean SIMARD


### Refactoring

[5add33](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5add330e9c715fc4164a22fbc9143241b3643656) - move all parsing into sub-modules - Jean SIMARD


- - -

This changelog was generated by [cocogitto](https://github.com/oknozor/cocogitto).