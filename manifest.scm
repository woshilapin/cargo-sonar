;; What follows is a "manifest" equivalent to the command line you gave.
;; You can store it in a file that you may then pass to any 'guix' command
;; that accepts a '--manifest' (or '-m') option.
(use-modules (guix packages)
             (guix profiles)
             (gnu packages commencement)
             (gnu packages llvm)
             (gnu packages mold)
             (gnu packages rust))

(packages->manifest (list clang
                          gcc-toolchain-14
                          mold
                          `(,rust "out")
                          `(,rust "cargo")
                          `(,rust "rust-src")
                          `(,rust "tools")
                          rust-analyzer))
